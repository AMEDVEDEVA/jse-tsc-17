package ru.tsc.golovina.tm.command.project;

import ru.tsc.golovina.tm.command.AbstractProjectCommand;
import ru.tsc.golovina.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.golovina.tm.model.Project;
import ru.tsc.golovina.tm.util.TerminalUtil;

public class ProjectUpdateByIdCommand extends AbstractProjectCommand {

    @Override
    public String getCommand() {
        return "project-update-by-id";
    }

    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public String getDescription() {
        return "Update project by id";
    }

    @Override
    public void execute() {
        System.out.println("Enter id");
        final String id = TerminalUtil.nextLine();
        final Project project = serviceLocator.getProjectService().findById(id);
        if (project == null) throw new ProjectNotFoundException();
        System.out.println("Enter name");
        final String name = TerminalUtil.nextLine();
        System.out.println("Enter description");
        final String description = TerminalUtil.nextLine();
        serviceLocator.getProjectService().updateById(id, name, description);
    }

    public static class ProjectRemoveByNameCommand extends AbstractProjectCommand {

        @Override
        public String getCommand() {
            return "project-remove-by-name";
        }

        @Override
        public String getArgument() {
            return null;
        }

        @Override
        public String getDescription() {
            return "Remove project by name";
        }

        @Override
        public void execute() {
            System.out.println("Enter name");
            final String name = TerminalUtil.nextLine();
            final Project project = serviceLocator.getProjectService().findByName(name);
            if (project == null) throw new ProjectNotFoundException();
            serviceLocator.getProjectTaskService().removeByName(name);
        }

    }

    public static class ProjectRemoveByIndexCommand extends AbstractProjectCommand {

        @Override
        public String getCommand() {
            return "project-remove-by-index";
        }

        @Override
        public String getArgument() {
            return null;
        }

        @Override
        public String getDescription() {
            return "Remove project by index";
        }

        @Override
        public void execute() {
            System.out.println("Enter index");
            final Integer index = TerminalUtil.nextNumber();
            final Project project = serviceLocator.getProjectService().findByIndex(index);
            if (project == null) throw new ProjectNotFoundException();
            serviceLocator.getProjectTaskService().removeByIndex(index);
        }

    }
}