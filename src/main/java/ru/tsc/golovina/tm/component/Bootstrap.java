package ru.tsc.golovina.tm.component;

import ru.tsc.golovina.tm.api.repository.ICommandRepository;
import ru.tsc.golovina.tm.api.repository.IProjectRepository;
import ru.tsc.golovina.tm.api.repository.ITaskRepository;
import ru.tsc.golovina.tm.api.service.*;
import ru.tsc.golovina.tm.command.AbstractCommand;
import ru.tsc.golovina.tm.command.project.*;
import ru.tsc.golovina.tm.command.system.*;
import ru.tsc.golovina.tm.command.task.*;
import ru.tsc.golovina.tm.exception.system.UnknownCommandException;
import ru.tsc.golovina.tm.model.Project;
import ru.tsc.golovina.tm.model.Task;
import ru.tsc.golovina.tm.repository.CommandRepository;
import ru.tsc.golovina.tm.repository.ProjectRepository;
import ru.tsc.golovina.tm.repository.TaskRepository;
import ru.tsc.golovina.tm.service.*;
import ru.tsc.golovina.tm.util.TerminalUtil;

public class Bootstrap implements ServiceLocator {

    private final ICommandRepository commandRepository = new CommandRepository();

    private final ICommandService commandService = new CommandService(commandRepository);

    private final IProjectRepository projectRepository = new ProjectRepository();

    private final IProjectService projectService = new ProjectService(projectRepository);

    private final ITaskRepository taskRepository = new TaskRepository();

    private final ITaskService taskService = new TaskService(taskRepository);

    private final IProjectTaskService projectTaskService = new ProjectTaskService(taskRepository, projectRepository);

    private final ILogService logService = new LogService();

    {
        registry(new CommandsDisplayCommand());
        registry(new AboutCommand());
        registry(new ExitCommand());
        registry(new HelpCommand());
        registry(new InfoCommand());
        registry(new VersionCommand());

        registry(new ProjectListShowCommand());
        registry(new ProjectClearCommand());
        registry(new ProjectCreateCommand());
        registry(new ProjectFinishByIdCommand());
        registry(new ProjectFinishByNameCommand());
        registry(new ProjectChangeStatusByIdCommand());
        registry(new ProjectChangeStatusByIndexCommand());
        registry(new ProjectChangeStatusByNameCommand());
        registry(new ProjectShowByIdCommand());
        registry(new ProjectShowByIndexCommand());
        registry(new ProjectShowByNameCommand());
        registry(new ProjectStartByIdCommand());
        registry(new ProjectStartByIndexCommand());
        registry(new ProjectStartByNameCommand());
        registry(new ProjectUpdateByIdCommand());
        registry(new ProjectUpdateByIndexCommand());

        registry(new TaskClearCommand());
        registry(new TaskCreateCommand());
        registry(new TaskFinishByIdCommand());
        registry(new TaskFinishByNameCommand());
        registry(new TaskListShowCommand());
        registry(new TaskRemoveByIdCommand());
        registry(new TaskRemoveByIndexCommand());
        registry(new TaskRemoveByNameCommand());
        registry(new TaskShowByIdCommand());
        registry(new TaskShowByIndexCommand());
        registry(new TaskShowByNameCommand());
        registry(new TaskStartByIdCommand());
        registry(new TaskStartByIndexCommand());
        registry(new TaskStartByNameCommand());
        registry(new TaskUpdateByIdCommand());
        registry(new TaskUpdateByIndexCommand());

        registry(new TaskListByProjectIdProjectTaskCommand());
        registry(new TaskAddToProjectByIdProjectTaskCommand());
        registry(new TaskRemoveFromProjectByIdProjectTaskCommand());
        registry(new ProjectUpdateByIndexCommand.RemoveByIdProjectCommand());
        registry(new ProjectUpdateByIdCommand.ProjectRemoveByIndexCommand());
        registry(new ProjectUpdateByIdCommand.ProjectRemoveByNameCommand());
    }

    public void start(final String... args) {
        displayWelcome();
        runArgs(args);
        initData();
        logService.debug("Test environment");
        while (true) {
            try {
                System.out.println("ENTER COMMAND");
                final String command = TerminalUtil.nextLine();
                logService.command(command);
                runCommand(command);
                logService.info("Completed");
            } catch (Exception e) {
                logService.error(e);
            }
        }
    }

    private void initData() {
        projectService.add(new Project("Project 3", "-"));
        projectService.add(new Project("Project 2", "-"));
        projectService.add(new Project("Project 1", "-"));
        projectService.add(new Project("Project 4", "-"));
        taskService.add(new Task("Task 1", "-"));
        taskService.add(new Task("Task 4", "-"));
        taskService.add(new Task("Task 3", "-"));
        taskService.add(new Task("Task 2", "-"));
        projectService.finishByName("Project 3");
        projectService.startByName("Project 1");
        taskService.finishByName("Task 3");
        taskService.startByName("Task 2");
    }

    private void displayWelcome() {
        System.out.println("---Welcome to task manager---");
    }

    private boolean runArgs(final String[] args) {
        if (args == null || args.length == 0) return false;
        AbstractCommand command = commandService.getCommandByName(args[0]);
        if (command == null) throw new UnknownCommandException(args[0]);
        command.execute();
        return true;
    }

    private void runCommand(final String command) {
        if (command == null || command.isEmpty()) return;
        AbstractCommand abstractCommand = commandService.getCommandByName(command);
        if (abstractCommand == null) throw new UnknownCommandException(command);
        abstractCommand.execute();
    }

    private void registry(AbstractCommand command) {
        if (command == null) return;
        command.setServiceLocator(this);
        commandService.add(command);
    }

    @Override
    public ITaskService getTaskService() {
        return taskService;
    }

    @Override
    public IProjectService getProjectService() {
        return projectService;
    }

    @Override
    public IProjectTaskService getProjectTaskService() {
        return projectTaskService;
    }

    @Override
    public ICommandService getCommandService() {
        return commandService;
    }

}
